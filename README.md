# Dental Assistant

Plataforma para Dentistas y Asistentes en un consultorio Dental.

* Gestión de Citas y Consultas

* Gestión de Pacientes y sus Expedientes Clínicos

* Registro de Pacientes

* Agenda de Citas

* Notificaciones de Cita para Paciente


## Hecho Con

* ASP.NET MVC (C#, Razor)
* Bootstrap 4