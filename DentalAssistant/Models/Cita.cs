﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DentalAssistant.Models
{

    public class Cita
    {
        [Key]
        public int id_cita { get; set; }

      
        [ForeignKey("id_paciente")]
        public Paciente paciente { get; set; }
        public int? id_paciente { get; set; }
        /*Asistente*/
       
        public string user_id  { get; set; }

        public DateTime fecha_hora { get; set; }
        public string metodo_recordatorio { get; set; }
    }
}