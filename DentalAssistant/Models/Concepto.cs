﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DentalAssistant.Models
{
    public class Concepto
    {
        [Key]
        public int id_concepto { get; set; }
        [Required]
        public string nombre { get; set; }
        public string descripcion { get; set; }
        
        public double costo { get; set; }
    }
}