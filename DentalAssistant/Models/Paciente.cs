﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DentalAssistant.Models
{
    public class Paciente
    {
        [Key]
        public int id_paciente { get; set; }

        [DisplayName("Nombre del Paciente")]
        public string Nombre { get; set; }

        [DisplayName("Sexo")]
        public char Sexo { get; set; }

        [DisplayName("Fecha de Nacimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? fecha_nac { get; set; }

        [DisplayName("Ciudad de Nacimiento")]
        public string Ciudad_nac { get; set; }

        [DisplayName("Estado de Nacimiento")]
        public string Estado_nac { get; set; }

        [DisplayName("Pais de Nacimiento")]
        public string Pais_nac { get; set; }

        [DisplayName("Escolaridad")]
        public string Escolaridad { get; set; }

        public string Domicilio { get; set; }

        [DisplayName("Codigo Postal")]
        public string zip_code { get; set; }

        [DisplayName("Numero Celular")]
        [Phone]
        public string Num_cel { get; set; }

        [DisplayName("Numero Telefono Fijo")]
        [Phone]
        public string Num_tel { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        


        [DisplayName("Fecha de Ingreso")]
        [DataType(DataType.Date)]
        public DateTime? fecha_ingreso { get; set; }

        //Seccion de antecedentes y personales

        public Boolean Alergias { get; set; } = false;

        public string Alergias_desc { get; set; }

        public string Alergias_ante { get; set; }

        public Boolean Cirugia { get; set; } = false;

        public string Cirugia_desc { get; set; }

        public Boolean Medicamento { get; set; } = false;

        public string Medicamento_desc { get; set; }

        public Boolean problema_anestesia { get; set; } = false;

        public string prob_anest_desc { get; set; }

        public Boolean Hemorragias { get; set; } = false;

        public string hemorragias_desc { get; set; }

        public Boolean Alcohol { get; set; } = false;

        public string Alcohol_desc { get; set; }

        public Boolean Drogas { get; set; } = false;

        public string Drogas_desc { get; set; }

        public Boolean Fuma { get; set; } = false;

        public string fuma_desc { get; set; }

        public Boolean Embarazo { get; set; } = false;

        public string Embarazo_desc { get; set; }

        //Enfermedades

        public Boolean Diabetes { get; set; } = false;
        public Boolean Hipertension { get; set; } = false;
        public Boolean Fiebre_reumatica { get; set; } = false;

        public Boolean Asma { get; set; } = false;

        [DisplayName("VIH/SIDA")]
        public Boolean AIDS { get; set; } = false;

        [DisplayName("Enfermedades Cardiovasculares")]
        public Boolean Enf_cardio { get; set; } = false;

        [DisplayName("Enfermedades Respiratorias")]
        public Boolean Enf_respiratorias { get; set; } = false;


    }
}