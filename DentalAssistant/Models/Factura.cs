﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DentalAssistant.Models
{
    public class Factura
    {
        [Key]
        [DisplayName("Folio Fiscal")]
        public int id_factura { get; set; }

        public string cadena_cert_sat { get; set; }
        public string sello_sat { get; set; }
        public string sello_digital { get; set; }

        public string no_serie_certificado { get; set; }
        [DisplayName("Fecha y hora de emisión")]
        public DateTime? fecha_hora { get; set; }
    }
}