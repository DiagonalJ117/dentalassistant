﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DentalAssistant.Models
{
    public class Consulta
    {
        [Key]
        public int id_consulta { get; set; }
        [ForeignKey("id_paciente")]
        public Paciente paciente { get; set; }
        public int? id_paciente { get; set; }
        /*Usuario*/
        public string user_id { get; set; }
        
        [ForeignKey("id_pago")]
        public Pago pago { get; set; }
        public int? id_pago { get; set; }

        [ForeignKey("id_cita")]
        public Cita cita { get; set; }
        public int? id_cita { get; set; }

        public DateTime hora_consulta { get; set; }

        [DisplayName("Razón de la Consulta")]
        public string razon_consulta { get; set; }

        public string Pronostico { get; set; }

        public float Temperatura { get; set; }

        public string Diagnostico { get; set; }

        public string Organo_dental { get; set; }

        public string Tratamiento { get; set; }

        public string Indicaciones { get; set; }


    }
}