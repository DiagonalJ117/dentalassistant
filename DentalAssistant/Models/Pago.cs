﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DentalAssistant.Models
{
    public class Pago
    {
        [Key]
        public int id_pago { get; set; }
        
        public virtual ICollection<Concepto> Conceptos { get; set; }

        [DisplayName ("Tipo de Pago")]
        public string tipo_pago { get; set; }

        public double importe { get; set; }

        public double total { get; set; }

        [ForeignKey("id_factura")]
        public Factura factura { get; set; }
        public int id_factura { get; set; }

    }
}