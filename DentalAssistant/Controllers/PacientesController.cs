﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DentalAssistant.Models;

namespace DentalAssistant.Controllers
{
    public class PacientesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Pacientes
        public ActionResult Index()
        {
            return View(db.Pacientes.ToList());
        }

        // GET: Pacientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paciente paciente = db.Pacientes.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }
            return View(paciente);
        }

        // GET: Pacientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pacientes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_paciente,Nombre,fecha_nac,Ciudad_nac,Estado_nac,Pais_nac,Escolaridad,Domicilio,zip_code,Num_cel,Num_tel,Email,fecha_ingreso,Alergias,Alergias_desc,Alergias_ante,Cirugia,Cirugia_desc,Medicamento,Medicamento_desc,problema_anestesia,prob_anest_desc,Hemorragias,hemorragias_desc,Alcohol,Alcohol_desc,Drogas,Drogas_desc,Fuma,fuma_desc,Embarazo,Embarazo_desc,Diabetes,Hipertension,Fiebre_reumatica,Asma,AIDS,Enf_cardio,Enf_respiratorias")] Paciente paciente)
        {
            if (ModelState.IsValid)
            {
                db.Pacientes.Add(paciente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(paciente);
        }

        // GET: Pacientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paciente paciente = db.Pacientes.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }
            return View(paciente);
        }

        // POST: Pacientes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_paciente,Nombre,fecha_nac,Ciudad_nac,Estado_nac,Pais_nac,Escolaridad,Domicilio,zip_code,Num_cel,Num_tel,Email,fecha_ingreso,Alergias,Alergias_desc,Alergias_ante,Cirugia,Cirugia_desc,Medicamento,Medicamento_desc,problema_anestesia,prob_anest_desc,Hemorragias,hemorragias_desc,Alcohol,Alcohol_desc,Drogas,Drogas_desc,Fuma,fuma_desc,Embarazo,Embarazo_desc,Diabetes,Hipertension,Fiebre_reumatica,Asma,AIDS,Enf_cardio,Enf_respiratorias")] Paciente paciente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paciente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(paciente);
        }

        // GET: Pacientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paciente paciente = db.Pacientes.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }
            return View(paciente);
        }

        // POST: Pacientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Paciente paciente = db.Pacientes.Find(id);
            db.Pacientes.Remove(paciente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
